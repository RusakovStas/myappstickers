//
//  ViewController.m
//  MyStickers
//
//  Created by Станислав Русаков on 29.06.17.
//  Copyright © 2017 Станислав Русаков. All rights reserved.
//

#import "ViewController.h"
#import "Facade.h"
#import "StickerTableViewCell.h"

#import <SDWebImage/UIImageView+WebCache.h>

static NSString * const kCellIdentifayer = @"StickerCell";

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) Facade *modelAPI;
@property (weak, nonatomic) IBOutlet UIButton *updateButton;

@end

@implementation ViewController

//TODO вынести сборку объектов в assembly
- (void)viewDidLoad {
    [super viewDidLoad];
    //Собираем фасад
    Facade *assembledModelAPI = [Facade sharedInstance];
    assembledModelAPI.controller = self;
    self.modelAPI = assembledModelAPI;
    
    [self.modelAPI loadStickers];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.parentViewController.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"common_bg"]];
    self.tableView.backgroundColor = [UIColor clearColor];
    UIEdgeInsets inset = UIEdgeInsetsMake(5, 0, 0, 0);
    self.tableView.contentInset = inset;
    
}

#pragma merk - UITableViewDataSource Implemetation

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.modelAPI.stickers.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    StickerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifayer];
    UIImage *background = [UIImage imageNamed:@"post-it-note-icons"];
    UIImageView *backroundView = [[UIImageView alloc] initWithImage:background];
    
    cell.backgroundView = backroundView;
    
    NSURL *urlSticker = [self.modelAPI getStickerAtIndex:self.modelAPI.stickers.count - 1 - indexPath.row ].stickerURL; //"инвертиртируем" индексы что бы новые загруженные были сверху (нельзя забывать что дубликаты стикеров не добавляются)
    NSLog(@"URL: %@",urlSticker);
    [cell.activityIndicator startAnimating];
    [cell.stickerImage sd_setImageWithURL:urlSticker
                         placeholderImage:[UIImage imageNamed:@"common_bg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                             [cell.activityIndicator stopAnimating];
                         }];
    
    
    return cell;

}

- (IBAction)tapUpdateButton:(id)sender {
    [self.modelAPI updateStickers];
}
#pragma mark - Controller Implementation

- (void)reloadDataInView{
    [self.tableView reloadData];
}

@end

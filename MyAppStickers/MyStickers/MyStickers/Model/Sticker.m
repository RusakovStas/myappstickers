//
//  Sticker.m
//  MyStickers
//
//  Created by Станислав Русаков on 01.07.17.
//  Copyright © 2017 Станислав Русаков. All rights reserved.
//

#import "Sticker.h"

@implementation Sticker

- (instancetype)initWithURL:(NSURL *)url {
    self = [super init];
    if (self) {
        _stickerURL = url;
    }
    return self;
}
- (instancetype)initWithID: (NSString *)stickerId{
    self = [super init];
    if (self) {
        _stickerID = stickerId;
        _stickerURL = [NSURL URLWithString:@" "];
    }
    return self;
}
- (instancetype)initWithID:(NSString *)stickerId
                    andURL:(NSURL *)url{
    self = [super init];
    if (self) {
        _stickerID = stickerId;
        _stickerURL = url;
    }
    return self;
}

@end

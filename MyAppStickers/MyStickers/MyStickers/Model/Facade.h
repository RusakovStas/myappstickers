//
//  Facade.h
//  MyStickers
//
//  Created by Станислав Русаков on 01.07.17.
//  Copyright © 2017 Станислав Русаков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Sticker.h"
#import "ServerDataSource.h"
#import "ModelProtocols/ComplitionOperation.h"
#import "ModelProtocols/Controller.h"

@interface Facade : NSObject <ComplitionOperation>

//Ссылка на Контроллер 
@property (nonatomic, weak) id<Controller> controller;

+ (Facade *)sharedInstance;


- (NSArray *)stickers;

- (void)loadStickers;

- (Sticker *)getStickerAtIndex:(int) index;

-(void)updateStickers;

@end

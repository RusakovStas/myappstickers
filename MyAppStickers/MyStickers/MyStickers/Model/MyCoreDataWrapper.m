//
//  MyCoreDataWrapper.m
//  MyStickers
//
//  Created by Станислав Русаков on 06.07.17.
//  Copyright © 2017 Станислав Русаков. All rights reserved.
//

#import "MyCoreDataWrapper.h"
#import "AppDelegate.h"
#import "CDStickerId+CoreDataClass.h"


@implementation MyCoreDataWrapper


#pragma mark - Other Methods


+(NSArray<CDStickerId *> *)getStickerIdsFromCoreData{
    NSFetchRequest <CDStickerId *> *fetchRequest = [CDStickerId fetchRequest];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray *stickerIdsEntitys = [[NSArray alloc] init];
    stickerIdsEntitys = [appDelegate.persistentContainer.viewContext
                  executeFetchRequest:fetchRequest
                  error:nil];
    NSMutableArray *stickerIds = [[NSMutableArray alloc] init];
    for (CDStickerId *stickerIdEntity in stickerIdsEntitys) {
        [stickerIds addObject:stickerIdEntity.stickerId];
    }
    return stickerIds;
}

+(void)saveStickerIdInCoreData:(NSString *)stickerForSave{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    NSEntityDescription *stickerIdDesc = [NSEntityDescription entityForName:@"CDStickerId"
                                                     inManagedObjectContext:context];
    
    CDStickerId *stickerId = [[CDStickerId alloc] initWithEntity:stickerIdDesc
                              insertIntoManagedObjectContext:context];
    stickerId.stickerId = stickerForSave;
    
    [appDelegate saveContext];
    
}


@end

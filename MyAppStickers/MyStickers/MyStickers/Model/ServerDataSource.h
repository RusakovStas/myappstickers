//
//  ServerDataSource.h
//  MyStickers
//
//  Created by Станислав Русаков on 02.07.17.
//  Copyright © 2017 Станислав Русаков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Sticker.h"
#import "LocalModel.h"

@protocol ServerDataSource <NSObject>


- (void)loadStickersFromServerInLocalModel;
//TODO Нужно подумать на счет этого метода
- (void)setTargetLocalModel:(id<LocalModel>) localModel;

@end

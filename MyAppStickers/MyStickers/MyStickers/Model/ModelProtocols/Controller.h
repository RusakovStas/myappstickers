//
//  Controller.h
//  MyStickers
//
//  Created by Станислав Русаков on 03.07.17.
//  Copyright © 2017 Станислав Русаков. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Controller <NSObject>
/**
 Данный метод предназначен для обновления данных из модели
 */
- (void)reloadDataInView;

@end

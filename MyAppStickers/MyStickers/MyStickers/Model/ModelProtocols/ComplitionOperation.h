//
//  ComplitionOperation.h
//  MyStickers
//
//  Created by Станислав Русаков on 02.07.17.
//  Copyright © 2017 Станислав Русаков. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ComplitionOperation <NSObject>

- (void)reloadData;

@end

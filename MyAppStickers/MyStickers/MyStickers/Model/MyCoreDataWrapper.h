//
//  MyCoreDataWrapper.h
//  MyStickers
//
//  Created by Станислав Русаков on 06.07.17.
//  Copyright © 2017 Станислав Русаков. All rights reserved.
//

#import <Foundation/Foundation.h>


@class CDStickerId;
@interface MyCoreDataWrapper : NSObject


+(NSArray<CDStickerId *> *)getStickerIdsFromCoreData;
+(void)saveStickerIdInCoreData:(NSString *)stickerForSave;

@end

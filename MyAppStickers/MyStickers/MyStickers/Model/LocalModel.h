//
//  LocalModel.h
//  MyStickers
//
//  Created by Станислав Русаков on 02.07.17.
//  Copyright © 2017 Станислав Русаков. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LocalModel <NSObject>


- (void)updateStickerUrl:(Sticker *)newSticker;
- (void)updateStickerIDs:(NSArray *)newIds withUpdateCoreData:(BOOL)needUpdate;

//Знаю что опциональные методы в протоколе это не хорошо, но в данном случае мне кажется эта возможность хорошо подходит, так как не всегда у стикеров есть id (или он не всегда будет нужен)
@optional
- (NSArray<NSString *> *)getStickersIds;

@end

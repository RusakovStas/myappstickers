//
//  StickersManager.m
//  MyStickers
//
//  Created by Станислав Русаков on 01.07.17.
//  Copyright © 2017 Станислав Русаков. All rights reserved.
//

#import "StickersManager.h"
#import "MyCoreDataWrapper.h"

@interface StickersManager (){

    NSMutableArray<Sticker *> *allStickers;

    NSMutableSet<NSString *> *stickerStruct;
    
}
@end

@implementation StickersManager

#pragma mark - Lazy initialization
- (NSMutableSet<NSString *> *)getStickerStruct{
    
    stickerStruct = [NSMutableSet new];
    if (allStickers) {
        for (Sticker *sticker in allStickers) {
            [stickerStruct addObject:sticker.stickerID];
        }
    }
    return stickerStruct;
}


#pragma mark - Initialization
- (instancetype)init
{
    self = [super init];
    if (self) {
        allStickers = [NSMutableArray new];
    }
    return self;
}

#pragma mark - Other methods

-(void)addSticker:(Sticker *)sticker{
    [allStickers addObject:sticker];
}

- (NSArray *)stickers {
    return allStickers;
}
- (Sticker *)getStickerAtIndex:(int) index{
    return allStickers[index];
}







#pragma mark - LocalModel Implementation


- (void)updateStickerUrl:(Sticker *)newSticker{
    for (Sticker *sticer in allStickers) {
        NSLog(@"Update sticker with id %@ and new url %@", sticer.stickerID, newSticker.stickerURL);
        if ([sticer.stickerID isEqualToString:newSticker.stickerID]) {
            
            sticer.stickerURL = newSticker.stickerURL;
        }
    }
}
/**
 Метод обновления id стикеров
 в случае поступления новых id - добавляем их в модель, а так же заносим их в Core Data
 */
- (void)updateStickerIDs:(NSArray *)newIds
      withUpdateCoreData:(BOOL)needUpdateCoreData{
    for (NSString *newId in newIds) {
        if (![[self getStickerStruct] containsObject:newId]) {
            [allStickers addObject:[[Sticker alloc] initWithID:newId]];
            if (needUpdateCoreData) {
                
                [MyCoreDataWrapper saveStickerIdInCoreData:newId];
            }
            
        }
    }
}

- (NSArray<NSString *> *)getStickersIds {
    return [[self getStickerStruct] allObjects];
}





@end

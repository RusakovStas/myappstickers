//
//  Sticker.h
//  MyStickers
//
//  Created by Станислав Русаков on 01.07.17.
//  Copyright © 2017 Станислав Русаков. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Sticker : NSObject

@property(nonatomic, strong)NSURL *stickerURL;
@property(nonatomic, strong)NSString *stickerID;

- (instancetype)initWithURL: (NSURL *)url;
- (instancetype)initWithID: (NSString *)stickerId;
- (instancetype)initWithID:(NSString *)stickerId andURL:(NSURL *)url;
@end

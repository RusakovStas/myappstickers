//
//  Facade.m
//  MyStickers
//
//  Created by Станислав Русаков on 01.07.17.
//  Copyright © 2017 Станислав Русаков. All rights reserved.
//

#import "Facade.h"
#import "MyHTTPClientForBotAPI.h"
#import "MyCoreDataWrapper.h"

#import "StickersManager.h" //импортируется только здесь и больше никому не виден


static NSString * const kMyBotToken = @"400613473:AAGi4vWFQNySNiuj0PISo7-cpLE43isbqIg";
//static NSString * const kMyBotToken = @"429705666:AAEiDQFKZaLKsJ3M23Nx6nag-pToJlUH76o"; //Для тестирования

@interface Facade (){
    StickersManager *sticManager;
    id<ServerDataSource> myHTTPClient;
}

@end

@implementation Facade

#pragma mark - Initialization
/**
 При инициализации объекта "фасада" производим сборку объекта модельного менеджера и соеденяем его с загрузчиком
 */
- (instancetype)init
{
    self = [super init];
    if (self) {
        //Инициализируем объект менеджера стикеров
        sticManager = [[StickersManager alloc] init];
        
        
        //Инициализируем объект для связи с сервером и указываем ему на менеджер стикеров
        //Весь прикол в том что теперь достаточно будет поменять данный объект и поменяется поведение без прочего переписывания
        
        myHTTPClient = [[MyHTTPClientForBotAPI alloc] initWithBotToken:kMyBotToken //Указываем токен бота с которого будем загружать данные
                                                        andTargetModel:sticManager //Указываем модель которую будем обновлять (должна удовлетворять протоколу LocalModel)
                        
                                        andComplitionOperationDelegate:self];   //Указываем что сами будем выполнять завершающую операцию по оканчание ассинхронной загрузки (этого действия могло бы и не быть )
        }
    return self;
}


#pragma mark - Singltone implementations
+(Facade *)sharedInstance {

    static Facade * _sharedInstance = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[Facade alloc] init];
    });
    return _sharedInstance;

}


#pragma mark - Other methods
-(void)addSticker:(Sticker *)sticker {
    [sticManager addSticker:sticker];
}

-(NSArray *)stickers{
    return [sticManager stickers];
}

- (Sticker *)getStickerAtIndex:(int) index{
   return [sticManager getStickerAtIndex:index];
}
/**
 Данный метод осуществляет загрузку стикеров по следующему алгоритму
 1. Загрузка данных из Core Data в локальную модель
 2. Запуск обновления данных в локальной модели через сервер - этот шаг производится в том числе и на основе данных из Core Data потому что URL предоставляемые Bot Api устаревают
 Локальная модель в текущей реализации отвечает за уникальность содержащихся в ней стикеров
 
 */
-(void)loadStickers{
    [sticManager updateStickerIDs:[MyCoreDataWrapper getStickerIdsFromCoreData] withUpdateCoreData:NO];
    
    [myHTTPClient loadStickersFromServerInLocalModel]; // Данный метод обновляет данные в модели согласно реализации HTTP клиента
}
/**
 Данный метод осуществляет обновление стикеров
 по сути он должен производиться HTTP клиентом, но в данном случае нам не за чем загружать данные из Core Data 
 
 */

-(void)updateStickers{
    
    [myHTTPClient loadStickersFromServerInLocalModel]; // Данный метод обновляет данные в модели согласно реализации HTTP клиента
}

#pragma mark - ComplitionOperation Implementation

-(void)reloadData{ //Этот метод вызывается из compition блока по загрузке данных
    [self.controller reloadDataInView]; //Пробрасываем его до view Controller который уже выполнит обновление данных
}


@end

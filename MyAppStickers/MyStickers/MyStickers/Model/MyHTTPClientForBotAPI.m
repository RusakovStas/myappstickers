//
//  MyHTTPClientForBotAPI.m
//  MyStickers
//
//  Created by Станислав Русаков on 02.07.17.
//  Copyright © 2017 Станислав Русаков. All rights reserved.
//

#import "MyHTTPClientForBotAPI.h"
#import <AFNetworking/AFNetworking.h>

static NSString * const kServerUrl = @"https://api.telegram.org/bot";
static NSString * const kStickerUrlPrefix = @"https://api.telegram.org/file/bot";


@interface MyHTTPClientForBotAPI ()

@property(nonatomic, strong)NSString *updateReq;
@property(nonatomic, strong)NSString *getFileReq;

@end


@implementation MyHTTPClientForBotAPI

#pragma mark - Initialization

- (instancetype)initWithBotToken:(NSString *)botToken
                  andTargetModel:(id<LocalModel>)targetModel
  andComplitionOperationDelegate:(id<ComplitionOperation>)delegateComplitionOperation
{
    self = [super init];
    if (self){
//        _filePaths = [[NSMutableArray alloc] init];
//        _fileIds = [[NSMutableArray alloc] init];
        _botToken                    = botToken;
        _targetModel                 = targetModel;
        _complitionOperationDelegate = delegateComplitionOperation;
    }
    return self;
}

#pragma mark - Lazy Initialization of propertys
-(NSString *)updateReq{
    if (!_updateReq) {
        _updateReq = [NSString stringWithFormat:@"%@%@/getUpdates",kServerUrl,self.botToken];
    }
    return _updateReq;
}

-(NSString *)getFileReq{
    if (!_getFileReq) {
        _getFileReq = [NSString stringWithFormat:@"%@%@/getFile?file_id=",kServerUrl,self.botToken];
    }
    return _getFileReq;
}
#pragma mark - ServerDataSource Implementation

-(void)setTargetLocalModel:(id<LocalModel>)localModel{
    self.targetModel = localModel;
}
/**
  ServerDataSource реализация метода
 Данный метод получает обновления через bot api 
 Передает эти обновления в локальную модель - она с ними работает согласно своей реализации
 И вызывает обновление всех URL из локальной модели
 */
- (void)loadStickersFromServerInLocalModel{

    NSURL *URL = [NSURL URLWithString:self.updateReq];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSMutableArray *fileIds = [[NSMutableArray alloc] init];
    [manager GET:URL.absoluteString parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSArray *updates = [responseObject valueForKey:@"result"];
        
        for (NSDictionary *update in updates) {
            //bug fix =) обнаружено что приложение крашится если боту скинуть текстовое сообщение 
            if([update[@"message"] valueForKey:@"sticker"]){ //Если содержит стикер - вообще нужно вынести парс json в другой класс
               [fileIds addObject:update[@"message"][@"sticker"][@"file_id"]];
            }
            
        }

        [self.targetModel updateStickerIDs:fileIds withUpdateCoreData:YES];
        [self loadStickersURLByFileID:[self.targetModel getStickersIds]];
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)loadStickersURLByFileID:(NSArray<NSString *> *) fileIds
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    dispatch_group_t serviceGroup = dispatch_group_create(); //
    for (NSString *fileId in fileIds) {
        NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", self.getFileReq, fileId]];
        dispatch_group_enter(serviceGroup);
        [manager GET:URL.absoluteString parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
            NSString *file_path = responseObject[@"result"][@"file_path"];
                Sticker *newSticker = [[Sticker alloc] initWithID:fileId
                                                           andURL:[NSURL URLWithString:
                                                                   [NSString stringWithFormat:@"%@%@/%@",
                                                                    kStickerUrlPrefix,
                                                                    self.botToken,
                                                                    file_path
                                                                    ]
                                                                   ]];
                [self.targetModel updateStickerUrl:newSticker];

            dispatch_group_leave(serviceGroup);
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            dispatch_group_leave(serviceGroup);
        }];
    }
    dispatch_group_notify(serviceGroup, dispatch_get_main_queue(), ^{
        [self.complitionOperationDelegate reloadData];
        NSLog(@"All done");
    });
}

#pragma mark - private methods





@end

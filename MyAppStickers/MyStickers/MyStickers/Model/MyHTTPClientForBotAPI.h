//
//  MyHTTPClientForBotAPI.h
//  MyStickers
//
//  Created by Станислав Русаков on 02.07.17.
//  Copyright © 2017 Станислав Русаков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerDataSource.h"
#import "LocalModel.h"
#import "ModelProtocols/ComplitionOperation.h"

@interface MyHTTPClientForBotAPI : NSObject <ServerDataSource>

@property(nonatomic, strong)NSString *botToken;

@property(nonatomic, weak)id<LocalModel> targetModel;//Модель которую будем обновлять данными с сервера
@property(nonatomic, weak)id<ComplitionOperation> complitionOperationDelegate; //Тот кто будет выполнять операцию при завершение загрузки

- (instancetype)initWithBotToken:(NSString *)botToken
                  andTargetModel:(id<LocalModel>)targetModel
  andComplitionOperationDelegate:(id<ComplitionOperation>) delegateComplitionOperation;

@end

//
//  StickersManager.h
//  MyStickers
//
//  Created by Станислав Русаков on 01.07.17.
//  Copyright © 2017 Станислав Русаков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Sticker.h"
#import "LocalModel.h"

@interface StickersManager : NSObject <LocalModel>

- (NSArray *)stickers;
- (void)addSticker:(Sticker *)sticker;
- (Sticker *)getStickerAtIndex:(int) index;




@end

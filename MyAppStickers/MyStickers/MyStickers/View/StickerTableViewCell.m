//
//  StickerTableViewCell.m
//  MyStickers
//
//  Created by Станислав Русаков on 04.07.17.
//  Copyright © 2017 Станислав Русаков. All rights reserved.
//

#import "StickerTableViewCell.h"

@interface StickerTableViewCell ()

@end

@implementation StickerTableViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    [self initialize];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)initialize
{
    // This code is only called once
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicator.center = self.stickerImage.center;
    self.activityIndicator.hidesWhenStopped = YES;
    [self.stickerImage addSubview:self.activityIndicator];
}

@end

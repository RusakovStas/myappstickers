//
//  StickerTableViewCell.h
//  MyStickers
//
//  Created by Станислав Русаков on 04.07.17.
//  Copyright © 2017 Станислав Русаков. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StickerTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *stickerImage;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


@end

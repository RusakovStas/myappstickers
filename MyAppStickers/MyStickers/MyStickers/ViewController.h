//
//  ViewController.h
//  MyStickers
//
//  Created by Станислав Русаков on 29.06.17.
//  Copyright © 2017 Станислав Русаков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model/ModelProtocols/Controller.h"

@interface ViewController : UIViewController <UITableViewDataSource, Controller>


- (void)reloadDataInView;

@end

